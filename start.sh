#!/bin/bash
echo Starting NGinx
service nginx start

echo Starting uWSGI
cd /flaskApp
uwsgi -s /tmp/uwsgi.sock --manage-script-name --mount /=application:application --chown-socket=nginx:nginx
