# Signup Demo Application

This application uses Python's Flask framework, NGINX and uWSGI daemon

This Python sample application is based on the [eb-py-flask-signup](https://github.com/awslabs/eb-py-flask-signup) sample. It has been modified to run on Amazon EC2 Container Service (ECS).

__You will need to adapt the instructions below to your environment__
e.g. maybe run `pip` instead of `pip3` etc.

__NOTE__: only tested on Linux! Do not expect all the dependencies to install successfully on windows!

## Prerequisites

Install Python 3:

```
apt-get install -y --no-install-recommends build-essential python3 python3-pip python3-dev python3-venv
```

Install `pipenv` in a virtual environment:

```
python3 -m venv venv
venv/bin/pip install pipenv
```

then you can run pipenv with `/venv/bin/pipenv`


## Local run 

To run / debug locally 

```zsh
# activate virtual environment
pipenv shell
# install requirements
pipenv install 
# run server
# or python3 application.py
python application.py 
```

### Virtualenv with pipenv

* To activate the python venv managed by pipenv, run `pipenv shell`
* You can verify if you're using the venv by looking at `which python`
* To deactivate the venv, run `exit`
* To run a script in the python venv managed by pipenv, run `pipenv run <script>`


## Configure nginx

We're assuming the application is at path `/flaskApp` and that the
appropriate Python version and `pipenv` are installed as above.

Set config file:

```
ln -s /flaskApp/nginx-app.conf /etc/nginx/conf.d/ 
```

Turn off default config:

```
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.ORIGINAL
```

Then install the Flask app from project root:

```
path/to/pipenv install 
```

Run start-up script:

```
chmod +x ./start.sh
path/to/pipenv run ./start.sh
```